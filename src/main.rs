// repeatadly starts a random puzzle in infinity mode and brute-forces it.
// implementation took about 1.5 hours, takes ~25 seconds per puzzle

// !! WARNING !!

// BEFORE you run it you should setup a kill switch. For instance, I just configured a global KDE
// shortcut with the following command: bash -c "kill -9 $(pgrep hexcells-infini)"

use autopilot::{
    alert,
    geometry::Point,
    mouse::{self, Button},
};
use std::{thread, time};

const DELAY: u64 = 75;

fn main() {

    loop {
        start_puzzle();

        //solve_puzzle();
        solve_puzzle_faster(); // WAY faster but sometimes misses the occasional cell.

        thread::sleep(time::Duration::from_millis(2000));
        back_to_menu();
    }

}

fn start_puzzle() {
    let random_seed_button = Point::new(750f64, 350f64);
    let start_button = Point::new(970f64, 930f64);

    mouse::move_to(random_seed_button).expect("Unable to move mouse to random seed button");
    mouse::click(Button::Left, None);
    mouse::move_to(start_button).expect("Unable to move mouse to start button");
    mouse::click(Button::Left, Some(2000)); // loading game takes some time
}

fn solve_puzzle() {
    //let start = Point::new(492f64, 160f64);
    let start_coords = (217f64, 175f64); // 482

    let distance_row = 61f64;
    let distance_col = 53f64;

    for col in 0 .. 30 { //25 {
        for row in 0 .. 15 {
            check(Point::new(
                start_coords.0 + col as f64 * distance_col,
                start_coords.1 + row as f64 * distance_row,
            ));
        }
    }

}

fn solve_puzzle_faster() {
    //let start = Point::new(492f64, 160f64);
    let start_coords = (217f64, 175f64); // 482

    let distance_row = 61f64;
    let distance_col = 53f64;

    for col in 0 .. 30 { //25 {
        for row in 0 .. 15 {
            check_left(Point::new(
                start_coords.0 + col as f64 * distance_col,
                start_coords.1 + row as f64 * distance_row,
            ));
        }
    }

    for col in 0 .. 30 { //25 {
        for row in 0 .. 15 {
            check_right(Point::new(
                start_coords.0 + col as f64 * distance_col,
                start_coords.1 + row as f64 * distance_row,
            ));
        }
    }

}

fn check(point: Point) {
    mouse::move_to(point).expect("Unable to move mouse to point");
    mouse::click(Button::Left, Some(20));
    thread::sleep(time::Duration::from_millis(DELAY));
    mouse::click(Button::Right, Some(20));
}

fn check_left(point: Point) {
    mouse::move_to(point).expect("Unable to move mouse to point");
    mouse::click(Button::Left, Some(20));
}

fn check_right(point: Point) {
    mouse::move_to(point).expect("Unable to move mouse to point");
    mouse::click(Button::Right, Some(20));
}

fn back_to_menu() {
    let menu_button = Point::new(950f64, 880f64);

    mouse::move_to(menu_button).expect("Unable to move mouse to menu button");
    mouse::click(Button::Left, Some(2500)); // loading menu takes some time
}
