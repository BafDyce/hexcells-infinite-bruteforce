# Bruteforcer for Hexcells Infinite

Repeatadly starts a random puzzle in infinity mode and brute-forces it. Before
running the bruteforcer, you must open the "infinite puzzles" menu.
Implementation took about 1.5 hours, takes ~25 seconds per puzzle

## !! WARNING !!

**BEFORE you run it you should setup a kill switch. For instance, I just configured a global KDE shortcut with the following command: bash -c "kill -9 $(pgrep hexcells-infini)"**

## Some notes

This is really stupid, as it doesnt look where cells are.
I just measured the offsets between cells and iterate over a large field (which is larger than the actual game field).
All values are obviously hardcoded for my system, so you need to find proper values for your system.
